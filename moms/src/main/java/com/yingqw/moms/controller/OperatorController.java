package com.yingqw.moms.controller;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.code.kaptcha.impl.DefaultKaptcha;

@RestController
@RequestMapping("/operator")
public class OperatorController {
	private static final Logger LOGGER = LoggerFactory.getLogger(OperatorController.class);
	
	@Autowired
	private DefaultKaptcha defaultKaptcha;

	@PostMapping("/token")
	public ResponseEntity<String> token() {
		return new ResponseEntity<>("hello", HttpStatus.OK);
	}
	
	@GetMapping(value = "/captcha", produces = "image/jpeg")
	public void captcha(HttpServletResponse response) throws IOException  {
        response.setHeader("Cache-Control", "no-store");    
        response.setHeader("Pragma", "no-cache");    
        response.setDateHeader("Expires", 0);
        
        String text = defaultKaptcha.createText();  
        ImageIO.write(defaultKaptcha.createImage(text), "jpg", response.getOutputStream());
	}
}
