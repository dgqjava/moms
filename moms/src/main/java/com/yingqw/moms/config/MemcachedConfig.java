package com.yingqw.moms.config;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.command.TextCommandFactory;
import net.rubyeye.xmemcached.impl.KetamaMemcachedSessionLocator;
import net.rubyeye.xmemcached.transcoders.SerializingTranscoder;

/**
 * memcached配置
 * @author 戴根泉
 *
 */
@Configuration
public class MemcachedConfig {
	
	@Value("${memcached.connectionPoolSize}")
	private int connectionPoolSize;
	@Value("${memcached.failureMode}")
	private boolean failureMode;
	@Value("${memcached.server.hosts}")
	private String hosts;
	@Value("${memcached.server.ports}")
	private String ports;
	@Value("${memcached.server.weights}")
	private String weights;
	
	@Bean
	public XMemcachedClientBuilder xMemcachedClientBuilder() {
		String[] hs = hosts.split(",");
		String[] ps = ports.split(",");
		String[] ws = weights.split(",");
		
		List<InetSocketAddress> addressList = new ArrayList<>();
		int[] weightList = new int[hs.length];
		for(int i = 0; i < hs.length; i++) {
			addressList.add(new InetSocketAddress(hs[i], Integer.parseInt(ps[i])));
			weightList[i] = Integer.parseInt(ws[i]);
		}
		
		XMemcachedClientBuilder builder = new XMemcachedClientBuilder(addressList, weightList);
		builder.setCommandFactory(new TextCommandFactory());
		builder.setSessionLocator(new KetamaMemcachedSessionLocator());
		builder.setTranscoder(new SerializingTranscoder());
		
		return builder;
	}

	@Bean(destroyMethod = "shutdown")
	public MemcachedClient memcachedClient(XMemcachedClientBuilder builder) {
		try {
			return builder.build();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}