package com.yingqw.moms.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 系统配置
 * @author 戴根泉
 *
 */
@Configuration
@ConfigurationProperties(prefix = "moms")
public class MomsConfig {
	private List<String> ignoreUrls;

	public void setIgnoreUrls(List<String> ignoreUrls) {
		this.ignoreUrls = ignoreUrls;
	}

	public List<String> getIgnoreUrls() {
		return ignoreUrls;
	}
}