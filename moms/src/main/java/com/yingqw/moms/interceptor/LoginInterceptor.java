package com.yingqw.moms.interceptor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.yingqw.moms.config.MomsConfig;

import net.rubyeye.xmemcached.MemcachedClient;

@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	protected MomsConfig momsConfig;

	@Autowired
	protected MemcachedClient memcachedClient;

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String uri = request.getRequestURI();
		for(String ignoreUrl : momsConfig.getIgnoreUrls()) {
			if(uri.matches(ignoreUrl)) {
				return true;
			}
		}
		responseJson(response, -1, "无权访问");
		return false;
	}

	private void responseJson(HttpServletResponse response, int code, String msg) {
		response.setContentType("application/json; charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			out.write("{\"code\":" + code + ",\"msg\":\"" + msg + "\"}");
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
